import {Component} from '@angular/core';
import {AppComponentService} from './app.component.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppComponentService]
})
export class AppComponent {
  public deleteAll;
  public initControls;
  public froalaElements: any;
  public froalaEditors: any;
  public isEditMode: boolean = false;

  constructor(public appComponentService: AppComponentService) {
    this.froalaElements = this.appComponentService.getInitialElementsState();
    this.froalaEditors = this.appComponentService.getInitialEditorsState();
  }

  public getEditorByName(name: string): any {
    return this.froalaEditors[name];
  };

  public handleChangeEditMode(state): void {
    this.isEditMode = state;

    if (!state) {
      this.froalaElements = this.appComponentService.getInitialElementsState();
    }
  }

  public initialize(initControls): void {
    this.initControls = initControls;
  }

  public sendChanges() {
    this.appComponentService.sendChanges(this.froalaElements);
  }
}
