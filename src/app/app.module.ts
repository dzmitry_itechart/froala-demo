import 'froala-editor/js/plugins.pkgd.min.js';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {HttpClientModule} from '@angular/common/http';
import {AppToggleComponent} from './shared/components/app-toggle/app-toggle.component';
import {FroalaImageViewDirective} from './shared/directives/froala-image-view.directive';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    AppToggleComponent,
    FroalaImageViewDirective
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
