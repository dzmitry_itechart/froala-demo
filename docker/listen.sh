#!/usr/bin/env bash
read MESSAGE
echo "PID: $$"
echo "$MESSAGE"

git --git-dir=/app/dev/.git fetch --all
git --git-dir=/app/dev/.git reset origin/master --hard
