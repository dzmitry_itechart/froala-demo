#!/usr/bin/env bash

echo "Prepare to start."
echo "Going to clone next: ${GIT_REPO_HTTPS}"

rm -rf /app/*
git clone ${GIT_REPO_HTTPS} /app/dev --depth=1
cd /app/dev || exit 13

# add simple listener
nohup socat -u tcp-l:4201,fork system:/tmp/listen.sh &

npm install
npm install -g @angular/cli@7.3.9

banner "Let's go!"

# here we assume it is started on 4200.
ng serve --host 0.0.0.0
